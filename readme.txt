This archive contains Python code and supporting files for the tasks of a Coursera MOOC. 

Course Title: Using Databases with Python

Instructor: Charles Severance

Provider: University of Michigan

Summary: 
This course will introduce you to the basics of the Structured Query Language (SQL) as well
as basic database design for storing data as part of a multi-step data gathering, analysis, and processing effort. The course will use SQLite3 as its database. We will also build web crawlers and multi-step data gathering and visualization processes, and will use the D3.js library to do basic data visualization. 
This course will cover Chapters 14-15 of the book 'Python for Informatics'.

Link: https://www.coursera.org/learn/python-databases/

Session Date: January 18th - February 14th 2016
