import sqlite3

conn = sqlite3.connect('emaildb.sqlite')
cur = conn.cursor() #initialize db cursor

cur.execute('''
DROP TABLE IF EXISTS Counts''')

cur.execute('''
CREATE TABLE Counts (org TEXT, count INTEGER)''')

fname = raw_input('Enter file name: ')
if ( len(fname) < 1 ) : fname = 'mbox.txt' #no input
fh = open(fname) #initialize file handler
for line in fh: #iterate through the lines in the file
    if not line.startswith('From: ') : continue
    pieces = line.split()
    email = pieces[1]
    domain = email.split('@')[1] #NiK: split emails on '@' and get second part
#   print email, '///', domain

    cur.execute('SELECT count FROM Counts WHERE org = ? ', (domain, ))
    row = cur.fetchone() #get one row - it's gonna be only one anyway
    if row is None:
        cur.execute('''INSERT INTO Counts (org, count)
                VALUES ( ?, 1 )''', (domain, ) )
    else :
        cur.execute('UPDATE Counts SET count=count+1 WHERE org = ?',
            (domain, ))

    conn.commit()

# https://www.sqlite.org/lang_select.html
sqlstr = 'SELECT org, count FROM Counts ORDER BY count DESC LIMIT 10'

print
print "Counts:"
for row in cur.execute(sqlstr) :
    print str(row[0]), row[1]

cur.close()
